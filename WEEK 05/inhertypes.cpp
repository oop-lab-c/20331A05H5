// By including more than one inheritence from the inheritences mentioned below will result hybrid inheritence 
#include<iostream>
using namespace std;
// parent_class
class parent1 {
public:
    void displayp1(){
    cout<<"HELLO parent1 !"<<endl;
    }
};
class parent2 {
public :
    void displayp2(){
    cout<<"Hello parent2 !"<<endl;
    }
};
// SIMPLE INHERITENCE
class child1 : public parent1{
    public :
    void displayc1()
    {
        cout<<"HELLO Child1 !"<<endl;
    }
};
// MULTIPLE INHERITENCE
class child2 : public parent1 , public parent2 {
    public :
    void displayc2()
    {
        cout<<"HELLO Child2 !"<<endl;
    }
};
// MULTILEVEL INHERITENCE
class child3 : public child1{
    public:
    void displayc3()
    {
        cout<<"HELLO Child3 !"<<endl;
    }
};
//HIERARCHIAL INHERITENCE
class child4 : public parent2{
    public :
    void displayc4()
    {
        cout<<"HELLO Child4 !"<<endl;
    }
};
class child5 : public parent2{
    public :
void displayc5()
    {
        cout<<"HELLO Child1 !"<<endl;
    }
};
int main()
{
    child1 objc1;
    child2 objc2;
    child3 objc3;
    child4 objc4;
    child5 objc5;
    cout<<"Simple Inheritence "<<endl;
    objc1.displayp1();
    cout<<"\nMultiple Inheritence "<<endl;
    objc2.displayp1();
    objc2.displayc2();
    objc2.displayp2();
    cout<<"\nMultilevel Inheritence "<<endl;
    objc3.displayc1();
    objc3.displayc3();
    cout<<"\nHierarchial Inheritence"<<endl;
    objc4.displayc4();
    objc4.displayp2();
    objc5.displayc5();
    objc5.displayp2();
}
