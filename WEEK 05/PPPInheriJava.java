class A
{
    public int z;
    protected int radius;
    private int a;
    public static void main(String[] args)
    {
        A obj = new A();
        obj.a=10;
        System.out.println("the value of a is "+obj.a);
    }
}
class B extends A
{
    void area(int r)
    {
        radius=r;
        int area;
        area=3*radius*radius;
        System.out.println("the radius of the circle is "+radius);
        System.out.println("the area is "+area);
    }
    public static void main(String[] args)
    {
        B obj1 = new B();
        obj1.area(15);
        obj1.z=25;
        System.out.println("the value of z is "+obj1.z);
    }
}