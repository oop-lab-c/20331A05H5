class bird
{
    void fly()
    {
        System.out.println("I can fly in the sky");
    }
}
class parrot extends bird
{
    void eat()
    {
        System.out.println("I eat guava");
    }
}
class childparrot extends parrot // multiple inheitance 
{
    public static void main(String[] args)
    {
        childparrot p = new childparrot();
        p.fly();
        p.eat();
    }
}
class crow extends bird // simple inheritance
{
    void colour()
    {
        System.out.println("my colour is black");
    }
    public static void main(String[] args)
    {
        crow c = new crow();
        c.fly();
        c.colour();
    }
}