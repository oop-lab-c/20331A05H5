interface GrandParent  
{   
default void display()   
{   
System.out.println("Hi......\n");   
}   
}   
interface Parent1 extends GrandParent  
{   
 default void m1()
 {
     System.out.println("Java.....\n");
 }
}   
interface Parent2 extends GrandParent  
{  
  default void m2()
  {
      System.out.println("OOPS....");
  }
}   
public class Child implements Parent1, Parent2  
{   
public static void main(String args[])   
{   
Child obj = new Child();   
obj.display(); 
obj.m1();
obj.m2();
}   
} 