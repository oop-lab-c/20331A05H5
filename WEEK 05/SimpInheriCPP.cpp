//simple inheritence
#include<iostream>
using namespace std;
// parent class
class arthOperations {
    public:
    void add (){
        float a,b;
        cout<<"Enter two numbers : "<<endl;
        cin>>a>>b;
        cout<<"sum : "<<a+b<<endl;
    }
    void sub (){
        float a,b;
        cout<<"Enter two numbers : "<<endl;
        cin>>a>>b;
        cout<<"subtraction : "<<a-b<<endl;
    }
};
// derived class
class multip : public arthOperations{
    public :
void multi (){
        float a,b;
        cout<<"Enter two numbers : "<<endl;
        cin>>a>>b;
        cout<<"Multiplication : "<<a*b<<endl;
    }
};
int main()
{
    multip obj;
    obj.add();
    obj.multi();
}
