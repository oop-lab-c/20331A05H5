//C++ program to demonstrate Operator Overloading in c++
//program to add 2 complex numbers
#include<iostream>
using namespace std;
class Complex 
{
    float real, imag;
public:
    //Default constructor to initialize real and imag to 0
    Complex() 
    {
        real = 0;
        imag = 0;
    }
    //Parameterized constructor to assign values to real and imag
    Complex(float r, float i) 
    {
        real = r;
        imag = i;
    }
    //operator function
    //overloading of binary operator +
    Complex operator + (Complex const &obj)
    {
        Complex temp;
        temp.real = real + obj.real;
        temp.imag = imag + obj.imag;
        return temp;
    }
    void display() 
    { 
        if(imag<0)
        {
        cout<<real<<imag<<"i"<<endl;
        }
        else
        {
        cout<<real<<"+"<<imag<<"i"<<endl;
        }
    }
};
int main()
{
    float r1,r2,i1,i2;
    cout<<"Enter first complex number real and imaginary parts :"<<endl;
    cin>>r1>>i1;
    cout<<"Enter second complex number real and imaginary parts :"<<endl;
    cin>>r2>>i2;
    Complex obj1(r1,i1);
    Complex obj2(r2,i2);
    cout<<"The two complex numbers are: "<<endl;
    obj1.display();
    obj2.display();
    //obj1 calls the operator function(impliit call) 
    //obj2 is passed as an argument to the function(explicit call)
    Complex obj3 = obj1 + obj2;
    cout<<"The result after adding two complex numbers: "<<endl;
    obj3.display();
    return 0;
}
