//Program to demonstrate method/function overloading using inheritance in java
import java.util.*;
class Base{
    void display()
    {
        System.out.println("Method of Base class is called");
        System.out.println("Hello World");
    }
}
//Multi Level Inheritance
class Derived1 extends Base{
    void display(int i1)
    {
        System.out.println("Method of Derived1 class of Base class is called");
        System.out.println("Integer is: "+i1);
    }
}
class Derived2 extends Derived1{
    void display(float f)
    {
        System.out.println("Method of Derived2 class of Base class is called");
        System.out.println("Floating point number is : "+f);
    }
}
class Derived3 extends Derived2
{
    void display(int i1,int i2)
    {
        System.out.println("Method of Derived3 class of Base class is called");
        System.out.println("Sum of two integers is : "+(i1+i2));
    }
}
class MethodOLInheriJava
{
    public static void main(String[] args)
    {
        Scanner input=new Scanner(System.in);
        System.out.println("Enter an integer : ");
        int i1=input.nextInt();
        System.out.println("Enter another integer : ");
        int i2=input.nextInt();
        System.out.println("Enter any floating point number : ");
        float f=input.nextFloat();
        Derived3 obj=new Derived3();
        obj.display();
        obj.display(i1);
        obj.display(f);
        obj.display(i1,i2);
    }
}