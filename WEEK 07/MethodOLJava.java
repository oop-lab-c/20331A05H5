//Program to demonstrate method/function overloading in java
import java.util.*;
class Demo
{
    //Based on number of parameters
    void fun()
    {
        System.out.println("Hello World");
    }
    void fun(int i1)
    {
        System.out.println("Displaying an integer :"+i1);
    }
    void fun(String str1)
    {
        System.out.println("Displaying a String :"+str1);
    }
    void fun(float f1)
    {
        System.out.println("Displaying a floating point number :"+f1);
    }
    void fun(int i1,String str1)
    {
        System.out.println("Concatenation of an integer and a String :"+(i1+str1));
    }

    //Based on data type of the parameters
    void fun(int i1,int i2)
    {
        System.out.println("Addition of two integers :"+(i1+i2));
    }
    void fun(String str1,String str2)
    {
        System.out.println("Concatenation of two Strings :"+(str1+str2));
    }
    void fun(float f1,float f2)
    {
        System.out.println("Addition of two floating point numbers :"+(f1+f2));
    }

    //Based on sequence of data type of parameters
    void fun(int i1,float f1)
    {
        System.out.println("Addition of an integer and floating point number :"+(i1+f1));
    }
    void fun(float f1, int i1)
    {
        System.out.println("Subtraction of an integer from floating point number :"+(f1-i1));
    }
}
class MethodOLJava{
public static void main(String[] args)
{
    Demo obj=new Demo();
    Scanner input=new Scanner(System.in);
    System.out.println("Enter any String :");
    String str1=input.nextLine();
    System.out.println("Enter another String :");
    String str2=input.nextLine();
    System.out.println("Enter an integer :");
    int i1=input.nextInt();
    System.out.println("Enter another integer :");
    int i2=input.nextInt();
    System.out.println("Enter a floating point number :");
    float f1=input.nextFloat();
    System.out.println("Enter another floating point number :");
    float f2=input.nextFloat();
    obj.fun();
    obj.fun(i1);
    obj.fun(str1);
    obj.fun(f1);
    obj.fun(i1,str1);
    obj.fun(i1,i2);
    obj.fun(str1,str2);
    obj.fun(f1,f2);
    obj.fun(i1,f1);
    obj.fun(f1,i1);
}
}