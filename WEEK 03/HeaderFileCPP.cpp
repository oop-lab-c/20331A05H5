//Program to display Area and Volume of box using #ifdef an #ifndef
#include"boxArea.h"         
#include"boxVolume.h"
using namespace std;

float len,wid,hei;      

int main()
{
    #ifdef len      
    #ifdef wid 
        #undef len      
        #undef wid 
    #endif
    #endif 
    #ifndef len     
    #ifndef wid
    #ifndef hei
        cout<<"Enter the length, width, height of box"<<endl;
        cin>>len>>wid>>hei;         
        boxArea(len,wid);          
        boxVolume(len,wid,hei);
    #endif 
    #endif
    #endif
}
