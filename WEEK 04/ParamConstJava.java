import java.util.Scanner;

import java.util.*;

class Student {
    public Student() {
        String collegeName = "MVGR";
        int collegeCode = 33;
        System.out.println(collegeName);
        System.out.println(collegeCode);
    }

    public Student(String fullName, double semPercentage) {
        System.out.println(fullName);
        System.out.println(semPercentage);
    }

    protected void finalize() {
        System.out.println("finalize method invoked");
    }

    public static void main(String[] args) {
        String a;
        double b;
        Scanner sc = new Scanner(System.in);
        System.out.println("ENTER NAME AND SEM PERCENTAGE");
        a = sc.nextLine();
        b = sc.nextDouble();
        Student objP = new Student(a, b);
        Student obj = new Student();
        obj.finalize();
    }

}
