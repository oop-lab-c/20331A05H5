class Student {
    public Student() {
        String fullName = "Dharma";
        double semPercentage = 81.8;
        int rollNum = 5;
        String collegeName = "MVGR";
        int collegeCode = 33;
        System.out.println(fullName);
        System.out.println(semPercentage);
        System.out.println(rollNum);
        System.out.println(collegeName);
        System.out.println(collegeCode);
    }

    protected void finalize() {
        System.out.println("finalize method invoked");
    }

    public static void main(String[] args) {
        Student obj = new Student();
        obj.finalize();
    }

}
