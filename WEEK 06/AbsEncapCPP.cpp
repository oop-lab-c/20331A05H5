//C++ program to demonstrate setters and getters in c++
#include<iostream>
using namespace std;
class AccessSpecifierDemo
{
    private: 
    int priVar;
    protected:
    int proVar;
    public:
    int pubVar;
    //set method to assign values for the member variables
    void setVar(int priValue,int proValue,int pubValue)
    {
        priVar=priValue;
        proVar=proValue;
        pubVar=pubValue;
    }
    //get method to display the values of the member variables
    void getVar()
    { 
        cout<<"Private Variable :"<<priVar<<endl;
        cout<<"Protected Variable :"<<proVar<<endl;
        cout<<"Public Variable :"<<pubVar<<endl;
    }
};
int main()
{
    int priValue,proValue,pubValue;
    AccessSpecifierDemo obj;
    //Taking the user input from the console
    cout<<"Enter the values for private, protected and public variables :"<<endl;
    cin>>priValue>>proValue>>pubValue;
    obj.setVar(priValue,proValue,pubValue);
    obj.getVar();
    return 0;
}
