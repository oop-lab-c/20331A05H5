//Java program to demonstrate setters and getters in java
import java.util.*;
class AccessSpecifierDemo
{
    private int priVar;
    protected int proVar;
    int pubVar;
    //set method to assign values for the member variables
    public void setVar(int priValue,int proValue,int pubValue)
    {
        priVar=priValue;
        proVar=proValue;
        pubVar=pubValue;
    }
    //get method to display the values of the member variables
    public void getVar()
    {
        System.out.println("Value of Private Variable :"+priVar);
        System.out.println("Value of Protected Variable :"+proVar);
        System.out.println("Value of Public Variable :"+pubVar);
    }  
}
class AbsEncapJava
{
    public static void main(String[] args)
    {
        AccessSpecifierDemo obj=new AccessSpecifierDemo();
        Scanner input=new Scanner(System.in);
        //Taking the user input from the console
        System.out.println("Enter the values of private, protected and public variables :");
        int priValue=input.nextInt();
        int proValue=input.nextInt();
        int pubValue=input.nextInt();
        obj.setVar(priValue,proValue,pubValue);
        obj.getVar();
    }
}